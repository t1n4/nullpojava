import java.io.*;
import java.util.*;

class Main {
	public static void main (String[] args) throws java.lang.Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Map<String, String> map = new HashMap<>();
		try {
			while(true) {
				String line = br.readLine();
				if(line == null || line.length() == 0) {
					break;
				}
				String[] keyValues = line.split(",");
				map.put(keyValues[0], keyValues[1]);
			}
		} finally {
			br.close();
		}
		if(map.get("java") != null){
			System.out.println(map.get("java").toUpperCase());
		} else {System.out.println("");}
		if(map.get("ruby") != null){
			System.out.println(map.get("ruby").toUpperCase());
		} else {System.out.println("");}
		if(map.get("go") != null){
			System.out.println(map.get("go").toUpperCase());
		} else {System.out.println("");}
	}
}